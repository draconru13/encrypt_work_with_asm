Title
.686
.MODEL FLAT, C
.STACK 256

.DATA

;a_asm dw 2
;b_asm dw 4

.CODE

;����� 
EXTRN x : WORD 
EXTRN x1 : WORD 
EXTRN x2 : WORD 
EXTRN remainder : WORD
EXTRN i1 : WORD
EXTRN binaryNumber1 : WORD
EXTRN remainder1 : WORD
EXTRN i2 : WORD
EXTRN binaryNumber2 : WORD
EXTRN remainder2 : WORD
EXTRN i3 : WORD
EXTRN binaryNumber3 : WORD

PUBLIC C bit_1_encrypt_1_asm
PUBLIC C bit_2_encrypt_1_asm
PUBLIC C bit_3_encrypt_1_asm



bit_1_encrypt_1_asm PROC far

while:
	cmp x, 0
	jne test
	jmp EndOf

test:
	mov ax, x
	mov di, 2
	div di
	mov si, ah; �������� remainder
	mov dx, i1
	mul dx
	mov esp, esi; �������� remainder*i1
	mov ebx, binaryNumber1
	add esp, ebx;
	mov binaryNimber1, esp; �������� binaryNumber1
	mov edi, 0
	mov eax, 0
	mov esi, 0

	mov ax, x
	mov di, 2
	div di
	mov x, al; �������� x /= 2
	mov eax, 0
	mov edi, 0

	mov dx, i1
	mov cx, 10
	mul cx
	mov i1, ecx
	
	mov edx, 0
	mov ecx, 0
	jmp while

EndOf:
	mov eax, 0
	mov ecx, 0
	mov ecx, binaryNimber1
	mov eax, ecx
	mov binaryNimber1, ecx
	retn

bit_1_encrypt_1_asm ENDP 




bit_2_encrypt_1_asm PROC far

while:
	cmp x1, 0
	jne test
	jmp EndOf

test:
	mov ax, x1
	mov di, 2
	div di
	mov si, ah; �������� remainder
	mov dx, i2
	mul dx
	mov esp, esi; �������� remainder*i2
	mov ebx, binaryNumber2
	add esp, ebx;
	mov binaryNimber2, esp; �������� binaryNumber1
	mov edi, 0
	mov eax, 0
	mov esi, 0

	mov ax, x1
	mov di, 2
	div di
	mov x1, al; �������� x1 /= 2
	mov eax, 0
	mov edi, 0

	mov dx, i2
	mov cx, 10
	mul cx
	mov i2, ecx
	
	mov edx, 0
	mov ecx, 0
	jmp while

EndOf:
	mov eax, 0
	mov ecx, 0
	mov ecx, binaryNimber2
	mov eax, ecx
	mov binaryNimber2, ecx
	retn

bit_2_encrypt_1_asm ENDP 




bit_3_encrypt_1_asm PROC far

while:
	cmp x2, 0
	jne test
	jmp EndOf

test:
	mov ax, x2
	mov di, 2
	div di
	mov si, ah; �������� remainder
	mov dx, i3
	mul dx
	mov esp, esi; �������� remainder*i3
	mov ebx, binaryNumber3
	add esp, ebx;
	mov binaryNimber3, esp; �������� binaryNumber1
	mov edi, 0
	mov eax, 0
	mov esi, 0

	mov ax, x2
	mov di, 2
	div di
	mov x2, al; �������� x2 /= 2
	mov eax, 0
	mov edi, 0

	mov dx, i3
	mov cx, 10
	mul cx
	mov i2, ecx
	
	mov edx, 0
	mov ecx, 0
	jmp while

EndOf:
	mov eax, 0
	mov ecx, 0
	mov ecx, binaryNimber3
	mov eax, ecx
	mov binaryNimber3, ecx
	retn

bit_3_encrypt_1_asm ENDP 
END 